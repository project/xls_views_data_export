<?php
/**
 * @file
 * Views include file with views hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function xls_views_data_export_views_plugins() {
  $path = drupal_get_path('module', 'xls_views_data_export');

  return array(
    'style' => array(
      'xls_views_data_export' => array(
        'title' => t('XLS file update'),
        'help' => t('Update existing XLS file.'),
        'handler' => 'xls_views_data_export_plugin_style_export',
        'export headers' => array('Content-Type' => 'application/vnd.ms-excel'),
        'export feed type' => 'xls',
        'export feed text' => 'XLS',
        'export feed file' => '%view.xls',
        'export feed icon' => drupal_get_path('module', 'views_data_export') . '/images/xls.png',
        'additional themes' => array(
          'xls_views_data_export_body' => 'style',
        ),
        'additional themes base' => 'xls_views_data_export',
        'path' => $path . '/plugins',
        'parent' => 'views_data_export_xls',
        'theme' => 'xls_views_data_export',
        'theme file' => 'xls_views_data_export.theme.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'data_export',
      ),
    ),
  );
}
