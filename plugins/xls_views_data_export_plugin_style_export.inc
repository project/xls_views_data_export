<?php

/**
 * @file
 * PHPExcel style plugin.
 */

class xls_views_data_export_plugin_style_export extends views_data_export_plugin_style_export {
  /**
   * Render the display in this style.
   */
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }

    $output = '';
    $output .= $this->render_body();
    return $output;
  }
  /**
   * Options form mini callback.
   *
   * @param $form
   * Form array to add additional fields to.
   * @param $form_state
   * State of the form.
   * @return
   * None.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['default_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Default file ID'),
      '#size' => 30,
      '#maxlength' => 60,
      '#autocomplete_path' => 'xls_views_data_export/autocomplete',
      '#default_value' => $this->options['default_file'],
    );
    $form['default_worksheet'] = array(
      '#type' => 'textfield',
      '#title' => t('Default Worksheet Name'),
      '#size' => 30,
      '#maxlength' => 60,
      '#default_value' => $this->options['default_worksheet'],
      '#description' => 'Disallowed Characters: * \ / : ? [ ]<br>Maximum Characters: 31',
    );
    $form['worksheet_override'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override Worksheet if it exists in workbook by default?'),
      '#default_value' => $this->options['worksheet_override'],
    );
  }
}
